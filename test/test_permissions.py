import os
import copy

import permissions.parse as pp
import permissions.compare as pc
import permissions.helpers as helpr


TEST_COLUMNS = [
    'Access Type',
    'Basic Permissions',
    'Access Scope',
    ]

TEST_COLUMN_INDECES = [6, 9, 10]

TEST_HEADER = [
    'Path',
    'Account Type',
    'Account Name',
    'Display Name',
    'Security Identifier',
    'Parent Group',
    'Access Type',
    'Inherited',
    'Owner',
    'Basic Permissions',
    'Access Scope',
    ''
]

TEST_ROW = [
    'C:\\Program Files',
    'Group',
    'BUILTIN\\Administrators',
    'Administrators',
    'S-1-5-32-544',
    '',
    'Allow',
    'False',
    'NT SERVICE\\TrustedInstaller',
    'Modify',
    'This folder only']

TEST_ROW_2 = [
    'C:\\Program Files\\Common Files',
    'Group', 'BUILTIN\\Administrators',
    'Administrators', 'S-1-5-32-544',
    '',
    'Allow',
    'False',
    'NT SERVICE\\TrustedInstaller',
    'Modify',
    'This folder only']

TEST_TABLE = [
    TEST_HEADER,
    ['C:\\Program Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Full control', 'Subfolders and files only'],  # noqa: E501
    ['C:\\Program Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Modify', 'This folder only'],  # noqa: E501
    ['C:\\Program Files', 'Group', 'APPLICATION PACKAGE AUTHORITY\\ALL APPLICATION PACKAGES', 'ALL APPLICATION PACKAGES', 'S-1-15-2-1', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Read and execute', 'This folder, subfolders, and files'],  # noqa: E501
    ['C:\\Program Files\\Box', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'FALSE', 'NT AUTHORITY\\SYSTEM', 'Full control', 'This folder, subfolders, and files']  # noqa: E501
]

TEST_TABLE_2 = [
    TEST_HEADER,
    ['C:\\Program Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Full control', 'Subfolders and files only'],  # noqa: E501
    ['C:\\Program Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Modify', 'This folder only'],  # noqa: E501
    ['C:\\Program Files', 'Group', 'APPLICATION PACKAGE AUTHORITY\\ALL APPLICATION PACKAGES', 'ALL APPLICATION PACKAGES', 'S-1-15-2-1', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Read and execute', 'This folder, subfolders, and files'],  # noqa: E501
    ['C:\\Program Files\\Box', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'FALSE', 'NT AUTHORITY\\SYSTEM', 'Full control', 'This folder, subfolders, and files'],  # noqa: E501
    ['C:\\Program Files\\Box\\Crapola', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'FALSE', 'NT AUTHORITY\\SYSTEM', 'Full control', 'This folder, subfolders, and files'],  # noqa: E501
    ['C:\\Program Files\\Common Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Allow', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Modify', 'This folder only'],  # noqa: E501
]

TEST_TABLE_PERMISSIONS = {
        'C:\\Program Files': {
            "S-1-5-32-544": [
                ('Allow', 'Full control', 'Subfolders and files only'),
                ('Allow', 'Modify', 'This folder only'),
            ],
            "S-1-15-2-1": [
                (
                    'Allow',
                    'Read and execute',
                    'This folder, subfolders, and files'),
            ],
        }, 'C:\\Program Files\\Box': {
            "S-1-5-32-544": [
                (
                    'Allow',
                    'Full control',
                    'This folder, subfolders, and files'),
            ],
        }}

TEST_PERMISSIONS_2 = {
        'C:\\Program Files': {
            "S-1-5-32-544": [
                ('Allow', 'Full control', 'Subfolders and files only'),
                ('Allow', 'Modify', 'This folder only'),
            ],
        }, 'C:\\Program Files\\Box': {
            "S-1-5-32-544": [
                (
                    'Allow',
                    'Full control',
                    'This folder, subfolders, and files'),
            ],
        }}

TEST_PERMISSIONS_WITH_DENY = {
    "S-1-5-32-544": [
        ('Allow', 'Full control', 'Subfolders and files only'),
        ('Deny', 'Modify', 'This folder only'),
    ]
}

TEST_TABLE_WITH_DENY = [
    TEST_HEADER,
    ['C:\\Program Files', 'Group', 'BUILTIN\\Administrators', 'Administrators', 'S-1-5-32-544', '', 'Deny', 'TRUE', 'NT SERVICE\\TrustedInstaller', 'Full control', 'Subfolders and files only'],  # noqa: E501
]

TEST_FOLDER_LIST = [
    'C:\\Program Files\\Box',
    'C:\\Program Files\\',
    'C:\\Program Files\\Common\ Files',
    'C:\\test',
]


class TestPermissions(object):
    def test_compare_increased_user_permissions(self):
        assert pc.compare_a_users_permissions(
            TEST_PERMISSIONS_2['C:\\Program Files'].get("S-1-5-32-544"),
            TEST_PERMISSIONS_2['C:\\Program Files\\Box'].get("S-1-5-32-544")
        ) == helpr.State.Increased

    def test_compare_decreased_user_permissions(self):
        assert pc.compare_a_users_permissions(
            TEST_PERMISSIONS_2['C:\\Program Files\\Box'].get("S-1-5-32-544"),
            TEST_PERMISSIONS_2['C:\\Program Files'].get("S-1-5-32-544")
        ) == helpr.State.Decreased

    def test_compare_equal_user_permissions(self):
        permission = \
            TEST_PERMISSIONS_2['C:\\Program Files'].get("S-1-5-32-544")
        assert pc.compare_a_users_permissions(
            permission, copy.deepcopy(permission)
        ) == helpr.State.Equal

    def test_permission_with_deny_returns_true(self):
        assert pp.check_for_denies_in_folder(TEST_PERMISSIONS_WITH_DENY)

    def test_compare_folder_permissions_of_test_table(self):
        permissions1 = TEST_TABLE_PERMISSIONS['C:\\Program Files']
        permissions2 = TEST_TABLE_PERMISSIONS['C:\\Program Files\\Box']
        (users_added_to_child, users_subbed_from_child) = \
            pc.check_for_diff_users(permissions1, permissions2)
        denied_on_either = \
            pp.check_for_denies_in_folder(permissions1) and \
            pp.check_for_denies_in_folder(permissions2)
        (inc_user_permissions, dec_user_permissions, special) = \
            pc.find_permission_changes_on_identical_users(
                permissions1, permissions2)
        assert not(users_added_to_child)
        assert users_subbed_from_child
        assert not(denied_on_either)

    def test_compare_identical_folder_permissions(self):
        permissions1 = TEST_TABLE_PERMISSIONS['C:\\Program Files']
        permissions2 = copy.deepcopy(permissions1)
        changes = pc.compare_folder_permissions(permissions1, permissions2)
        # (users_added_to_child, users_subbed_from_child, denied_on_either)
        assert changes == (False, False, False, False, False, False)

    def test_find_deny_in_tables(self):
        (header, table_no_deny) = helpr.header_and_table_from_array(
            TEST_TABLE)
        (header, table_with_deny) = helpr.header_and_table_from_array(
            TEST_TABLE_WITH_DENY)
        assert helpr.count_of_value_in_column(table_no_deny, 6, 'Deny') == 0
        assert helpr.count_of_value_in_column(table_with_deny, 6, 'Deny') == 1

    def test_find_one_broken_inheritance_in_test_table(self):
        (header, data) = helpr.header_and_table_from_array(
            TEST_TABLE)
        folder_list = pp.find_broken_inheritance(
            data, 0, 7)
        assert len(folder_list) == 1
        assert folder_list[0] == ('C:\\Program Files\\Box')

    def test_find_2_folders_in_test_table(self):
        (header, data) = helpr.header_and_table_from_array(
            TEST_TABLE)
        folder_permissions = pp.extract_user_permissions_from_table(
            data,
            0,
            4,
            TEST_COLUMN_INDECES)
        assert len(folder_permissions) == 2

    def test_reads_19174_rows_from_test_file(self):
        data = helpr.array_from_csv_file(
            os.path.join('data', 'Permissions Unfiltered.csv'))
        assert len(data) == 19175

    def test_getting_indeces_from_test_header(self):
        assert helpr.get_column_indeces_from_header(
            TEST_HEADER, TEST_COLUMNS) == TEST_COLUMN_INDECES

    def test_create_permission_from_row(self):
        permission = pp.get_permission_from_row(
            TEST_ROW,
            helpr.get_column_indeces_from_header(
                TEST_HEADER, TEST_COLUMNS
            )
        )
        assert permission == ('Allow', 'Modify', 'This folder only')

    def test_create_user_permissions_from_rows(self):
        (header, data) = helpr.header_and_table_from_array(
            TEST_TABLE)
        folder_permissions = pp.extract_user_permissions_from_table(
            data,
            0,
            4,
            TEST_COLUMN_INDECES)
        assert folder_permissions == TEST_TABLE_PERMISSIONS

    def test_folder_count_of_test_tables_works(self):
        (header, table) = helpr.header_and_table_from_array(
            TEST_TABLE)
        (header, table2) = helpr.header_and_table_from_array(
            TEST_TABLE_2)
        assert len(helpr.unique_column_values(table, 0)) == 2
        assert len(helpr.unique_column_values(table2, 0)) == 4

    def test_subfolders_found_correctly(self):
        (header, table) = helpr.header_and_table_from_array(
            TEST_TABLE_2)
        folders = helpr.unique_column_values(table, 0)
        assert len(pp.subfolders_for('C:\\Program Files', folders)) == 4
        assert len(pp.subfolders_for('sdfasdf', folders)) == 0

    def test_subpath_count_in_paths_are_correct(self):
        assert helpr.depth_of('C:\\Program Files\\Box', '\\') == 3
        assert helpr.depth_of('C:\\Program Files\\Box\\', '\\') == 3
        assert helpr.depth_of('C:', '\\') == 1

    def test_subfolder_elimination(self):
        assert pp.eliminate_subfolders(TEST_FOLDER_LIST) == \
            [
                'C:\\Program Files\\',
                'C:\\test',
            ]

    def test_subtree_counting(self):
        assert pp.count_folders_underneath(
            TEST_FOLDER_LIST,
            [
                'C:\\Program Files\\',
                'C:\\Program Files\\Common\ Files',
            ]) == 3
        assert pp.count_folders_underneath(
            TEST_FOLDER_LIST,
            [
                'C:\\Program Files\\Common\ Files',
                'C:\\test',
            ]) == 2
