# Permissions

Given a csv-stylee output from the 'Permissions Reporter' tool, this
    parses the output and reports on various aspects of the folder
    permissions. Most of the reporting is on the differences in a
    folder's permission set between a parent and a child folder, where
    the permission inheritance is broken between parent and child.

Being that the original spec only called for simple operations on
    pairs of folders and folder names, the folder structure is 
    internally represented as a dictionary of folders, and 
    folders themselves indexed and found as strings.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* python3

### Running

```
git clone https://billcodding@bitbucket.org/billcodding/permissions.git
cd permissions
python3 permissions.py -[f|p] data/xxxxx.csv
```
Command line options:

* -f: print summary of findings at all inheritance breaks
* -p: also print all permission diffs at all inheritance breaks

## Built With

* [flake8](http://flake8.pycqa.org) - Linted
* [pytest](https://docs.pytest.org) - Tested

## Authors

* **Bill Codding**
