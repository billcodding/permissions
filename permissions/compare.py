import permissions.parse as pp
from permissions.helpers import Enum as Enum

PERMISSION_LEVELS = {
    "Full control": 4,
    "Modify": 3,
    "Write": 2,
    "Read and execute, Write": 2,
    "Read and execute": 1,
    "Read": 0,
    "List folder contents": -1,
}


State = Enum(['Increased', 'Decreased', 'Equal', 'Special', 'Unknown'])


def compare_folder_permissions(folder_perms_parent, folder_perms_child):
    (users_added_to_child, users_subbed_from_child) = \
        check_for_diff_users(folder_perms_parent, folder_perms_child)
    denied_on_either = \
        pp.check_for_denies_in_folder(folder_perms_parent) and \
        pp.check_for_denies_in_folder(folder_perms_child)
    (
        increased_user_permissions,
        decreased_user_permissions,
        special_user_permissions
    ) = \
        find_permission_changes_on_identical_users(
            folder_perms_parent, folder_perms_child)
    return (
        users_added_to_child,
        users_subbed_from_child,
        denied_on_either,
        increased_user_permissions,
        decreased_user_permissions,
        special_user_permissions
        )


def check_for_diff_users(folder_perms_parent, folder_perms_child):
    users_added_to_child = False
    users_subbed_from_child = False
    """
    Look through the users in the permissions dictionary for
        each of two folders, and see which are not present in
        the other
    """

    # see if there are any users in the parent permissions
    #   that don't appear in the child - this implies that
    #   the user was removed from the child
    for user_id, permissions in folder_perms_parent.items():
        if user_id not in folder_perms_child:
            users_subbed_from_child = True
            break
    # see if there are any users in the child permissions
    #   that don't appear in the parents - this means that
    #   there were permissions added to the child
    #   (as long as they were 'Allow's, not 'Deny's)
    for user_id, permissions in folder_perms_child.items():
        if user_id not in folder_perms_parent:
            if pp.check_for_any_allow_in_user(
                    permissions):
                users_added_to_child = True
                break
    return(users_added_to_child, users_subbed_from_child)


def compare_a_users_permissions(folder_perms_parent, folder_perms_child):
    """
    Compare two lists of (permission triples) for a user for one
        particular folder, by:
        - finding the permission that corresponds to this folder, then
        - comparing the numerical levels assigned to the permission values
    """
    parent_permission = \
        pp.user_permission_for_this_folder(folder_perms_parent)
    child_permission = \
        pp.user_permission_for_this_folder(folder_perms_child)

    # catch a wierd situation where someone can have permissions for
    #   subfolders and files, but none at all for the folder itself
    if child_permission is None:
        if parent_permission is None:
            return State.Equal
        else:
            return State.Decreased

    parent_permission_level = parent_permission[1]
    child_permission_level = child_permission[1]

    return compare_user_permission_level(
        parent_permission_level, child_permission_level)


def compare_user_permission_level(
        parent_permission_level, child_permission_level):
    """
    Compare the numerical permission level for two individual permission
        levels.
    """
    # catch any 'Special' permissions - these just won't be compared
    if 'pecial' in parent_permission_level or \
            'pecial' in child_permission_level:
        return State.Special

    numerical_level_child = PERMISSION_LEVELS.get(child_permission_level)
    numerical_level_parent = PERMISSION_LEVELS.get(parent_permission_level)
    if numerical_level_child is None:
        print('** Unknown basic permission: "' + child_permission_level + '"'
              + ' - ignoring')
        return State.Unknown
    if numerical_level_parent is None:
        print('** Unknown basic permission: "' + parent_permission_level + '"'
              + ' - ignoring')
        return State.Unknown

    if numerical_level_child > numerical_level_parent:
        return State.Increased
    if numerical_level_child < numerical_level_parent:
        return State.Decreased
    if numerical_level_child == numerical_level_parent:
        return State.Equal


def find_permission_changes_on_identical_users(
            folder_perms_parent, folder_perms_child):
    """
    Go thru all of the users in two folder permission sets, and for
        all of the users common to both folders, find out if any
        users' permissions have increased or decreased
    """
    increased_user_permissions = False
    decreased_user_permissions = False
    special_user_permissions = False
    for user_id, permissions in folder_perms_parent.items():
        if user_id in folder_perms_child:
            user_perms_parent = folder_perms_parent[user_id]
            user_perms_child = folder_perms_child[user_id]
            state = compare_a_users_permissions(
                user_perms_parent, user_perms_child)
            if state == State.Increased:
                increased_user_permissions = True
            if state == State.Decreased:
                decreased_user_permissions = True
            if state == State.Special:
                special_user_permissions = True
            if state == State.Unknown:
                pass
    return(
        increased_user_permissions,
        decreased_user_permissions,
        special_user_permissions)
