import csv
import pprint


class Enum(tuple):
    __getattr__ = tuple.index


State = Enum(['Increased', 'Decreased', 'Equal', 'Special'])


def array_from_csv_file(filepath):
    return list(csv.reader(open(filepath, errors='ignore')))


def header_and_table_from_array(data):
    header = data[0]
    table = data[1:]
    return (header, table)


def get_column_index_from_header(header, column):
    return header.index(column)


def get_column_indeces_from_header(header, columns):
    indeces = []
    for column in columns:
        indeces.append(header.index(column))
    return indeces


def count_of_value_in_column(table, column_index, value):
    return [row[column_index] for row in table].count(value)


def parent_name(folder_name, separator):
    path_elements = folder_name.split(separator)
    last_element = path_elements.pop()
    if last_element == '':
        # folder name ended with a separator, take another one off!
        path_elements.pop()
    return separator.join(path_elements)


def unique_column_values(table, index):
    return set([row[index] for row in table])


def depth_of(filepath, separator):
    depth = len(filepath.split(separator))
    if filepath.endswith(separator):
        depth -= 1
    return depth


def print_permissions(folder_name, folder_permissions):
    prp = pprint.PrettyPrinter()
    print('folder: ', folder_name)
    print()
    print('permissions: ')
    prp.pprint(folder_permissions)
