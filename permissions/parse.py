import copy


def get_permission_from_row(row, indeces_of_interesting_columns):
    permission = []
    for index in indeces_of_interesting_columns:
        permission.append(row[index])
    return tuple(permission)


def extract_user_permissions_from_table(
        table,
        index_of_folder_name,
        index_of_security_id,
        indeces_of_permission_components):

    all_permissions = dict()

    for row in table:
        folder_name = row[index_of_folder_name]
        security_id = row[index_of_security_id]
        permission = get_permission_from_row(
            row, indeces_of_permission_components)

        folder_permissions = all_permissions.get(folder_name)
        if folder_permissions is None:
            # create a new dictionary entry for this folder name
            all_permissions[folder_name] = {security_id: [permission, ]}
        else:
            # an entry for this folder exists; add the new permission
            # tuple to the folder's user dictionary
            add_permission_to_user_dictionary(
                folder_permissions, security_id, permission)

    return all_permissions


def add_permission_to_user_dictionary(
        folder_permissions, security_id, permission):
    """
    Add the new permission tuple to a user dictionary if it exists.
    """

    user_permissions = \
        folder_permissions.get(security_id)

    if user_permissions is None:
        # this user doesn't yet have an entry for this folder; create it
        folder_permissions[security_id] = [permission, ]
    else:
        user_permissions.append(permission)


def user_permission_for_this_folder(users_permissions):
    """
    Each user has a list of one or more permissions for a folder; those
        permissions can be for that folder, its subfolders, or files...
        here the task is to find the one that applies to 'This folder' itself
    """
    permission_for_this_folder = None
    for perm_triple in users_permissions:
        if 'This folder' in perm_triple[2]:
            permission_for_this_folder = perm_triple
            break
    return permission_for_this_folder


def find_broken_inheritance(table, folder_index, broken_index):
    folders_with_broken_inheritance = []
    for row in table:
        folder_name = row[folder_index]
        inherited_string = row[broken_index]
        inherited = False if inherited_string.lower() == 'false' else True
        if not(inherited):
            folders_with_broken_inheritance.append(folder_name)
    unique_folders = list(set(folders_with_broken_inheritance))
    return unique_folders


def count_folders_underneath(all_folders, folders):
    n_folders_affected = 0
    topmost_folders = eliminate_subfolders(folders)
    for folder_name in topmost_folders:
        subfolders = subfolders_for(folder_name, all_folders)
        n_folders_affected += len(subfolders)
    return n_folders_affected


def check_for_denies_in_folder(folder_permissions):
    denied = False
    for user_id, user_permissions in folder_permissions.items():
        for permission in user_permissions:
            if permission[0] == 'Deny':
                denied = True
                break
    return denied


def check_for_any_allow_in_user(user_permissions):
    any_allows = False
    for permission in user_permissions:
        if permission[0] == 'Allow':
            any_allows = True
            break
    return any_allows


def subfolders_for(folder_name, folder_list):
    return [folder for folder in folder_list if folder_name in folder]


def eliminate_subfolders(folder_list):
    """
    Find the top-most folders in the directory structure that are in the
        folder list. This involves checking folders one by one to see
        if they are either a parent of other folders in the tree (and then
        eliminating those other folders and adding the new one), or if they
        are a child of another folder and eliminating the child.

        (Note that if the folder structure had been done with a tree
        instead of a dict, this might have been easier!)
    """
    list_without_subfolders = []
    for folder in folder_list:
        folder_is_parent = False
        folder_is_child = False
        for prospective_parent in copy.deepcopy(list_without_subfolders):
            if folder in prospective_parent:
                list_without_subfolders.remove(prospective_parent)
                folder_is_parent = True
                continue
            if prospective_parent in folder:
                folder_is_child = True
                break
        if (folder_is_parent) or \
                (not(folder_is_child) and not(folder_is_parent)):
            list_without_subfolders.append(folder)
    return list_without_subfolders
