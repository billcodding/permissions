"""
Given a csv-stylee output from the 'Permissions Reporter' tool, this
    parses the output and reports on various aspects of the folder
    permissions. Most of the reporting is on the differences in a
    folder's permission set between a parent and a child folder, where
    the permission inheritance is broken between parent and child.

    Folders are represented as strings, parents of folders as
        substrings, e.g.:
        'C:\\parent\\child', 'C:\\parent'
    Permissions are a tuple of:
        Access Type (Allow/Deny)
        Basic Permission (Read/Read and Execute/Modify, ...)
        Scope (This folder/Subfolder and files/This folder, subfolder, ...)
        e.g. ('Allow', 'Read', 'This folder')
    A user's permissions for a folder are a list of permission tuples:
        e.g. [  ('Allow', 'Read', 'This folder'),
                ('Allow', 'Write', 'Subfolders and files') ]
    A folder's permission set is a dictionary of user id's and their perms:
        e.g. {  'S-1-3-0':  [(...), (...)]
                'S-1-5-18': [(...)] }
    The folder structure is a dictionary of directory names and the perm set:
        e.g. {  'C:\\User':         {...}
                'C:\\User\\Jones':  {...}    }

    Notes:
    - This was written in almost exclusively procedural style using
        tuples and lists as simple objects for simplicity
    - As the original spec had no operations that necessitated walking
        the directory tree, the folder structure is represented as
        a dictionary rather than a tree; most ops were possible
        just looking up individual folders rather than needing a tree
        representation.
    - However, late in the game a req was added to
        find subfolders, which would have been far more efficient walking
        a folder tree...if rewritten, neither of the above might be the
        chosen style!

    Author: Bill Codding 2018-05-01
"""

import copy
import operator
import sys

import permissions.parse as pp
import permissions.compare as pc
import permissions.helpers as helpr


USAGE = \
    'Usage: python3 permissions [-f|p] filename' + '\n' + \
    '  -f: print summary of findings at all inheritance breaks' + '\n' + \
    '  -p: also print all permission diffs at all inheritance breaks' + '\n'

COLUMN_FOLDER = 'Path'

COLUMN_SECURITY_ID = 'Security Identifier'

COLUMN_BROKENNESS = 'Inherited'

COLUMN_ALLOW_DENY = 'Access Type'

COLUMNS_PERMISSIONS = [
    'Access Type',
    'Basic Permissions',
    'Access Scope',
]


print_folder_by_folder_summary = False
print_folder_perm_diffs = False
args = copy.deepcopy(sys.argv)
# print(args)
if '-p' in args:
    print_folder_by_folder_summary = True
    args.remove('-p')
if '-f' in args:
    print_folder_perm_diffs = True
    args.remove('-f')
if len(args) < 2 or args[1].startswith('-'):
    print(USAGE)
    exit()
filename = args[1]
# filename = "data/Greyco_1.csv"

# read the permissions reporter csv output into a header and table
data = helpr.array_from_csv_file(filename)
(header, table) = helpr.header_and_table_from_array(data)

# find the indeces of the columns we're interested in
index_folder = \
    helpr.get_column_index_from_header(header, COLUMN_FOLDER)
security_id_index = \
    helpr.get_column_index_from_header(header, COLUMN_SECURITY_ID)
index_broken = \
    helpr.get_column_index_from_header(header, COLUMN_BROKENNESS)
index_allow = \
    helpr.get_column_index_from_header(header, COLUMN_ALLOW_DENY)
permission_column_indeces = \
    helpr.get_column_indeces_from_header(header, COLUMNS_PERMISSIONS)

# extract folder/user/permissions
permissions = pp.extract_user_permissions_from_table(
        table,
        index_folder,
        security_id_index,
        permission_column_indeces)

all_folders = helpr.unique_column_values(table, index_folder)

folders_with_broken_inheritance = \
    pp.find_broken_inheritance(table, index_folder, index_broken)

folders_affected_by_broken_inheritance = pp.count_folders_underneath(
        all_folders, folders_with_broken_inheritance)

total_no_findings = 0
totals = [0, 0, 0, 0, 0, 0]
total_identical = 0
folder_names_with_subbed_users = []
folder_names_with_decreased_perms = []

# find the interesting stuff about parent/child folder pairs, where the
#   permission is not inherited from the parent
for folder_name in folders_with_broken_inheritance:
    folder_permissions = permissions.get(folder_name)

    # check for the edge case where some folder's parent is not
    #   in the permissions reporter dump (e.g. the top-most
    #   level folder)
    parent_name = helpr.parent_name(folder_name, '\\')
    if parent_name in permissions:
        parent_folder_permissions = permissions.get(parent_name)
    else:
        # try the name with a trailing slash, we've seen that once
        parent_name = parent_name + '\\'
        if parent_name in permissions:
            parent_folder_permissions = permissions.get(parent_name)
        else:
            continue

    results = pc.compare_folder_permissions(
            parent_folder_permissions,
            folder_permissions)

    (
        users_added_to_child,
        users_subbed_from_child,
        denied_on_either,
        increased_user_permissions,
        decreased_user_permissions,
        special_user_permissions
    ) = results

    # stash the folder names away where interesting stuff was found:
    if users_subbed_from_child:
        folder_names_with_subbed_users.append(folder_name)
    if decreased_user_permissions:
        folder_names_with_decreased_perms.append(folder_name)

    # convert all the booleans to integers, and add them to the totals:
    found = list(map(int, list(results)))
    totals = list(map(operator.add, totals, found))
    if(sum(found) == 0):
        total_no_findings += 1

    # do a separate related check - are the permissions on the folders
    #   actually identical?
    if(parent_folder_permissions == folder_permissions):
        total_identical += 1

    if print_folder_by_folder_summary or print_folder_perm_diffs:
        print('------------------------------------------------------------')
        print('folder:', folder_name)
        print('parent', parent_name)
        print('differences:')
        print("    'Deny' permission on either: ", denied_on_either)
        print('    users added to child:', users_added_to_child)
        print('    users subbed from child:', users_subbed_from_child)
        print('    increased user permission on child:',
              increased_user_permissions)
        print('    decreased user permissions on child: ',
              decreased_user_permissions)
        print('    special user permissions on either: ',
              special_user_permissions)
        print()
        if print_folder_perm_diffs:
            helpr.print_permissions(parent_name, parent_folder_permissions)
            helpr.print_permissions(folder_name, folder_permissions)
        print('------------------------------------------------------------')

folders_affected_by_subbed_users = pp.count_folders_underneath(
        all_folders, folder_names_with_subbed_users)
folders_affected_by_decreased_perms = pp.count_folders_underneath(
        all_folders, folder_names_with_decreased_perms)

print()
print('SUMMARY:')
print('total folders: ', len(permissions))
print('folders with \'Deny\':',
      helpr.count_of_value_in_column(table, index_allow, 'Deny'))
print('folders with disinheritance: ', len(folders_with_broken_inheritance))
print('    users_added_to_child:', totals[0])
print('    users_subbed_from_child:', totals[1])
print('    denied_on_either:', totals[2])
print('    increased_user_permissions:', totals[3])
print('    decreased_user_permissions:', totals[4])
print('    special_user_permission:', totals[5])
print('    none of the above:', total_no_findings)
print('    identical permissions:', total_identical)
print('number of folders affected by:')
print('    broken inheritance:', folders_affected_by_broken_inheritance)
print('    subbed users:', folders_affected_by_subbed_users)
print('    decreased perms:', folders_affected_by_decreased_perms)
